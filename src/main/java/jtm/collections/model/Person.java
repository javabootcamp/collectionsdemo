package jtm.collections.model;

import java.time.LocalDate;
import java.util.Objects;

public class Person implements Comparable<Person> {

    private String firstName;
    private String lastName;
    private String personalCode;
    private LocalDate dateOfBirth;

    public Person(String firstName, String lastName, String personalCode, LocalDate dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.personalCode = personalCode;
        this.dateOfBirth = dateOfBirth;
    }

    public Person(String firstName, String lastName, String personalCode) {
        this(firstName, lastName, personalCode, null);
    }

    @Override
    /*
        Equals method should return true if this object is considered equal as object o
        Object o can be of type person but it also can be of other type
     */
    public boolean equals(Object o) {
        //First we check that this is completely the same object as o (stored in the same place in memory)
        //If that's the case return true - no need to check anything else
        if (this == o) return true;
        //Then we check if this class is the same as type of object o from parameter
        //If both types are not the same return false, no need to check anything else
        if (o == null || getClass() != o.getClass()) return false;
        //If classes are the same we can cas Object o to the same type as this class is
        Person person = (Person) o;
        //Invoke compareTo method, check rules how it works below
        int equalsResult = compareTo(person);
        //If compareTo method returns 0 then we consider both objects to be equal if not then not equal
        return  equalsResult == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, personalCode, dateOfBirth);
    }


    @Override
    /*
        compareTo method returns integer
        If returned value is 0 - that means that this person is considered equal
            (or the same) as person o that's passed to method parameter
        If returned value is negative - that means that this person is considered 'smaller' as person o.
            If person is 'smaller' it will be put first when sorting
        If returned value is positive - that means that this person is considered 'larger' as a person o.
            If person is 'larger' it will be put after this person when sorting
     */
    public int compareTo(Person o) {
        if (!this.firstName.equals(o.lastName)) {
            return this.firstName.compareTo(o.firstName);
        }
        if (!this.lastName.equals(o.lastName)) {
            return this.lastName.compareTo(o.lastName);
        }
        if (!this.personalCode.equals(o.personalCode)) {
            return this.personalCode.compareTo(o.personalCode);
        }
        if (!this.dateOfBirth.equals(o.dateOfBirth)) {
            return this.dateOfBirth.compareTo(o.dateOfBirth);
        }
        return 0;
    }


    //Getters and Setters
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", personalCode='" + personalCode + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}

