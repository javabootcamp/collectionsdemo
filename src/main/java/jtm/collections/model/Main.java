package jtm.collections.model;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        //testStringList();
        //testPersonMap();
        //testPersonList();
    }

    private static void testStringList() {

        //Create NEW ArrayList to store String objects
        List<String> myArrayList = new ArrayList<>();

        //Add persons to list
        myArrayList.add("Orange");
        myArrayList.add("Lemon");
        myArrayList.add("Watermelon");
        myArrayList.add("Kiwi");

        //To check how many objects are in list
        int size = myArrayList.size();
        System.out.println("Size of list after initialization is " + size);

        //To iterate over list with indexes
        System.out.println("Starting to iterate over list with standard for..");
        for(int i=0; i < myArrayList.size(); i++){
            String someString = myArrayList.get(i);
            System.out.println("String with index " + i + " is " + someString);
        }
        System.out.println("Done iterating over list with standard for..");

        //To sort LIST according to compareTo rules in Person
        Collections.sort(myArrayList);

        //To iterate over list using list-iterator
        ListIterator<String> iterator = myArrayList.listIterator();
        System.out.println("Starting to iterate over list with iterator..");
        while(iterator.hasNext()){
            String someString =iterator.next();
            System.out.println("String from iterator " + someString);
        }
        System.out.println("Done iterating over list with iterator..");


    }


    private static void testPersonList() {
        Person john = new Person("John", "Doe", "290190-10000");
        Person jane = new Person("Jane", "Doe", "140292-10000");
        Person cnut = new Person("Cnut", "Doe", "040493-10000");
        Person edward = new Person("Edward", "Doe", "100690-10000");

        //Create NEW ArrayList to store Person objects
        List<Person> persons = new ArrayList<>();

        //Add persons to list
        persons.add(john);
        persons.add(jane);
        persons.add(cnut);
        persons.add(edward);

        //To check how many objects are in list
        int size = persons.size();
        System.out.println("Size of list after initialization is " + size);

        //Add person with specific index
        //Before operation list looks like: [John (0), Jane (1), Cnut (2), Edward(3)]
        Person mia = new Person("Mia", "Doe", "110992-10000");
        persons.add(2,mia);
        //After operation list looks like: [John (0), Jane (1), Mia(2), Cnut (3), Edward(4)]

        //To get person with index 3, Cnut in this case
        Person personIndex3 = persons.get(3);
        System.out.println("Person with index 3 is " + personIndex3.getFirstName());

        //To iterate over list with indexes
        System.out.println("Starting to iterate over list with standard for..");
        for(int i=0; i < persons.size(); i++){
            Person p = persons.get(i);
            System.out.println("Person with index " + i + " is " + p.getFirstName());
        }
        System.out.println("Done iterating over list with standard for..");

        //To sort LIST according to compareTo rules in Person
        Collections.sort(persons);

        //To iterate over list with short-hand for
        System.out.println("Starting to iterate over list with short-hand for..");
        for(Person p: persons){
            System.out.println("Persons perosnal code is " + p.getPersonalCode());
        }
        System.out.println("Done iterating over list with short-hand for..");

        //To iterate over list using list-iterator
        ListIterator<Person> iterator = persons.listIterator();
        System.out.println("Starting to iterate over list with iterator..");
        while(iterator.hasNext()){
            Person p =iterator.next();
            System.out.println("Person from iterator " + p.getFirstName() + " " + p.getPersonalCode());
        }
        System.out.println("Done iterating over list with iterator..");


        //Remove all persons form list
        System.out.println("List size before clear " + persons.size());
        persons.clear();
        System.out.println("List size after clear " + persons.size());

    }

    private static void testPersonMap() {
        Person john = new Person("John", "Doe", "290190-10000");
        Person jane = new Person("Jane", "Doe", "140292-10000");
        Person cnut = new Person("Cnut", "Doe", "040493-10000");
        Person edward = new Person("Edward", "Doe", "100690-10000");

        //Create NEW HashMap, with key type String and value type Person
        Map<String, Person> persons = new HashMap<>();

        //Put person objects into map, use personal code as key (unique value among persons)
        persons.put(john.getPersonalCode(), john);
        persons.put(jane.getPersonalCode(), jane);
        persons.put(cnut.getPersonalCode(), cnut);
        persons.put(edward.getPersonalCode(), edward);

        //To check how many objects are in map
        int size = persons.size();
        System.out.println("Size of map after initialization is " + size);

        //To check if map has object mapped to key
        String personalCode = "290190-10000";
        boolean containsPersonWithKey = persons.containsKey(personalCode);
        System.out.println("Does map contain value with key " + personalCode + " ? Result = " + containsPersonWithKey);

        //To get value from map by key (personal code)
        Person personFromMap = persons.get(personalCode);
        System.out.println("Found person in map by key " + personalCode + " person: " + personFromMap);

        //To remove value from map by key
        persons.remove(personalCode);
        System.out.println("Size of map is " + persons.size() + " after removing entry with key " + personalCode);

        //To iterate over map
        System.out.println("Iterating over map values with short-hand for..");
        for(Person person : persons.values()){
            System.out.println("Persons name is " + person.getFirstName());
        }
        System.out.println("Finished iterating over map values with short-hand for..");

        System.out.println("Iterating over map keys to get values with short-hand for..");
        for(String key : persons.keySet()){
            Person p = persons.get(key);
            System.out.println("Persons name is " + p.getFirstName());
        }
        System.out.println("Finished iterating over map keys with short-hand for..");

        //Remove all entries from map
        System.out.println("Map size before clear " + persons.size());
        persons.clear();
        System.out.println("Map size after clear " + persons.size());
    }


}
